**Nombre:** Luis Angel Correa Ramírez
**Correo:** lacorrea819@misena.edu.co - luisanimation3d@gmail.com

# Resolución del problema

Primero revisé la descripción de la prueba y las reglas del negocio, posteriormente me dí a la tarea de buscar referencias para realizar un diseño, tanto de las cards del catálogo, las cards del carrito y el detalle del producto; algunas de estas referencias fueron Mercado Libre, Apple, Wacom, Samsung, etc.

Una vez inspirado realizé un boceto en papel, luego de hacer diferentes pruebas ya en código, logré terminar los diseños, asegurandome que fueran minimalistas y fáciles de procesar para la vista del cliente, además de intuitivos.

Una vez terminado el diseño(vistas) comenzé a realizar la funcionalidad del aplicativo, después de pensar varias formas de llevar los productos seleccionados al carrito, decidí optar por el localStorage, ya que es algo que había utilizado para un apartado similar en un proyecto anterior, después de terminar las funcionalidades y las correspondientes validaciones, llegó el momento de la prueba final, es decir, revisar que todo esté funcionando como debería.

# Instalar proyecto

Para el proyecto se usaron algunas librerias extra, las cuales fueron
- **React Router DOM** para manejar las rutas, y lo manejé en la versión 5.3.0
- **Axios** para consumir la API local, y lo manejé en la versión 0.24.0

Para la instalación del proyecto se deben seguir los siguientes pasos:

- Clonar el repositorio de GitHub corriendo el comando git clone https://luisanimation3d@bitbucket.org/luisanimation3d/luis-angel-test-front-end.git
- Acceder por medio de CLI(Consola de comandos) a la carpeta root del proyecto utilizando el cd front-end-tech-test, hay que recordar que primero hay que acceder a la carperta donde hayas descargado el proyecto.
- Una vez dentro de la carpeta root, ejecutamos el comando npm install para instalar las dependencias de la API local y esperamos.
- Una vez terminado la instalación, accedemos a la carpeta front ejecutando la linea de comando cd front.
- Estando dentro de esta carpeta necesitamos las dependencias para el front end para eso ejecutamos nuevamente npm install, aqui se instala las dependencias mencionadas anteriormente ya que se encuentran en el archivo package.json
- Cuando termine la instalación volvemos a la carpeta root usando el comando cd ..
- Ahora podemos ejecutar el proyecto utilizando el comando npm run dev y lo abrimos en el navegador.
