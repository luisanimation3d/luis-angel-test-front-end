import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import Catalogo from './Componentes/catalogo';
import ShoppingCar from './Componentes/carrito';
import ShowDetails from './Componentes/show_details';

class Rutas extends Component {
    render() {
        return (
            <Switch>
                <Route path="/catalogo" component={Catalogo}></Route>
                <Route path="/shopping_car" component={ShoppingCar}></Route>
                <Route path="/product/:id" component={ShowDetails}></Route>
            </Switch>
        );
    }
}

export default Rutas;
