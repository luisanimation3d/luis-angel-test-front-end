import React, { useState, useEffect } from 'react';
import { BrowserRouter, Link } from "react-router-dom";
import Rutas from './Routes';
import './App.css';

const App = () => {
  // -------------------------------------------------
  // DO NOT USE THE CODE BELOW FROM LINES 8 TO 18. THIS IS
  // HERE TO MAKE SURE THAT THE EXPRESS SERVER IS RUNNING
  // CORRECTLY. DELETE CODE WHEN COMPLETING YOUR TEST.
  const [response, setResponse] = useState('')

  // call server to see if its running
  useEffect(() => {
    const getApiResponse = () => {
      fetch('http://localhost:5000/')
        .then((res) => res.text())
        .then((res) => setResponse(res))
    }
    getApiResponse()
  }, [])
  // -------------------------------------------------

  return (
    <div className="Nav">
      <BrowserRouter>
        <Link className="nav-item" to="/catalogo">| Catalogue |</Link>
        <Link className="nav-item" to="/shopping_car"> Cart |</Link>
        <div><Rutas /></div>

      </BrowserRouter>
    </div>
  )
}

export default App
