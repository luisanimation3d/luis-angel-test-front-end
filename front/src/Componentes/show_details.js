import React, { Component } from 'react';

import axios from 'axios';

import '.././css/product_details.css';

class Show_Details extends Component {

    constructor(props) {
        super(props);
        this.state = {
            product: [],
            message: "",
        }
    }

    componentDidMount() {
        axios({
            method: 'get',
            url: `http://localhost:5000/api/products/${this.props.match.params.id}`,
        }).then((answer) => {
            this.setState({
                product: answer.data
            })
        }).catch(error => {
            this.setState({
                message: "The requested product was not found"
            })
        });
    }

    Verify(id) {
        var productos = localStorage.Shopping_car === undefined ? [] : JSON.parse(localStorage.Shopping_car);
        if (productos.length !== 0) {
            var resultado = productos.find(function (e) { return e.id === id; });
            var amount = document.querySelector("#amount" + id).value;
            if (amount >= 1) {
                amount = parseFloat(amount);
            } else {
                amount = 1;
            }
            if (resultado !== undefined) {
                var position = productos.findIndex(function (e) { return e.id === id; });
                var verify_amount = parseFloat(productos[position].amount);
                verify_amount = verify_amount + amount;
                if (productos[position].stock >= verify_amount) {
                    var amount_new = parseFloat(productos[position].amount);
                    productos[position].amount = amount_new + amount;
                    productos[position].total = parseFloat(productos[position].amount) * parseFloat(productos[position].price);
                    localStorage.Shopping_car = JSON.stringify(productos);
                    alert("Added product");
                } else {
                    alert("Not enough stock");
                }
            } else {
                this.Add_to_car(id);
            }
        } else {
            this.Add_to_car(id);
        }
    };

    Add_to_car(id) {
        var productos = localStorage.Shopping_car === undefined ? [] : JSON.parse(localStorage.Shopping_car);
        var category = document.querySelector("#category" + id).value;
        var brand = document.querySelector("#brand" + id).value;
        var img = document.querySelector("#img" + id).value;
        var price = document.querySelector("#price" + id).value;
        var name = document.querySelector("#name" + id).value;
        var description = document.querySelector("#description" + id).value;
        var stock = document.querySelector("#stock" + id).value;
        var amount = document.querySelector("#amount" + id).value;
        var total_price = parseFloat(amount) * parseFloat(price);
        if (amount >= 1) {
            amount = amount;
        } else {
            amount = 1;
        }
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for (var i = 0; i < 15; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        productos.push({
            'id': id,
            'category': category,
            'brand': brand,
            'img': img,
            'price': price,
            'name': name,
            'description': description,
            'stock': stock,
            'result': result,
            'amount': amount,
            'total': total_price
        });
        localStorage.Shopping_car = JSON.stringify(productos);
        alert("Added Product");
        window.location.reload();
    };

    Product() {
        if (this.state.product.length !== 0) {
            return (
                <div className="card_shopping_detail">
                    <input type="hidden" value={this.state.product.description} id={`description${this.state.product._id}`} />
                    <input type="hidden" value={this.state.product.countInStock} id={`stock${this.state.product._id}`} />
                    <input type="hidden" value={this.state.product.brand} id={`brand${this.state.product._id}`} />
                    <input type="hidden" value={this.state.product.category} id={`category${this.state.product._id}`} />
                    <input type="hidden" value={this.state.product.image} id={`img${this.state.product._id}`} />
                    <input type="hidden" value={this.state.product.price} id={`price${this.state.product._id}`} />
                    <input type="hidden" value={this.state.product.name} id={`name${this.state.product._id}`} />
                    <img src={`http://localhost:5000${this.state.product.image}`} alt={this.state.product.name} />
                    <h2 className="product_name_detail">{this.state.product.name}</h2>
                    <p className="product_description_detail">{this.state.product.description}</p>
                    <div className="icons_details">
                        <span className="icon_rating_details"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                            <path d="M8.864.046C7.908-.193 7.02.53 6.956 1.466c-.072 1.051-.23 2.016-.428 2.59-.125.36-.479 1.013-1.04 1.639-.557.623-1.282 1.178-2.131 1.41C2.685 7.288 2 7.87 2 8.72v4.001c0 .845.682 1.464 1.448 1.545 1.07.114 1.564.415 2.068.723l.048.03c.272.165.578.348.97.484.397.136.861.217 1.466.217h3.5c.937 0 1.599-.477 1.934-1.064a1.86 1.86 0 0 0 .254-.912c0-.152-.023-.312-.077-.464.201-.263.38-.578.488-.901.11-.33.172-.762.004-1.149.069-.13.12-.269.159-.403.077-.27.113-.568.113-.857 0-.288-.036-.585-.113-.856a2.144 2.144 0 0 0-.138-.362 1.9 1.9 0 0 0 .234-1.734c-.206-.592-.682-1.1-1.2-1.272-.847-.282-1.803-.276-2.516-.211a9.84 9.84 0 0 0-.443.05 9.365 9.365 0 0 0-.062-4.509A1.38 1.38 0 0 0 9.125.111L8.864.046zM11.5 14.721H8c-.51 0-.863-.069-1.14-.164-.281-.097-.506-.228-.776-.393l-.04-.024c-.555-.339-1.198-.731-2.49-.868-.333-.036-.554-.29-.554-.55V8.72c0-.254.226-.543.62-.65 1.095-.3 1.977-.996 2.614-1.708.635-.71 1.064-1.475 1.238-1.978.243-.7.407-1.768.482-2.85.025-.362.36-.594.667-.518l.262.066c.16.04.258.143.288.255a8.34 8.34 0 0 1-.145 4.725.5.5 0 0 0 .595.644l.003-.001.014-.003.058-.014a8.908 8.908 0 0 1 1.036-.157c.663-.06 1.457-.054 2.11.164.175.058.45.3.57.65.107.308.087.67-.266 1.022l-.353.353.353.354c.043.043.105.141.154.315.048.167.075.37.075.581 0 .212-.027.414-.075.582-.05.174-.111.272-.154.315l-.353.353.353.354c.047.047.109.177.005.488a2.224 2.224 0 0 1-.505.805l-.353.353.353.354c.006.005.041.05.041.17a.866.866 0 0 1-.121.416c-.165.288-.503.56-1.066.56z" />
                        </svg>{this.state.product.rating} </span>
                        <span className="icon_numReviews_details"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                            <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z" />
                            <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
                        </svg>{this.state.product.numReviews}</span>
                    </div>
                    <p className="product_details_detail"><span>Brand:</span> {this.state.product.brand} <span>Category:</span> {this.state.product.category}</p>
                    <h3 className="price_detail">${this.state.product.price}</h3>
                    <div className="amount_details"><input type="number" placeholder="Seleccione la cantidad deseada" defaultValue="1" id={`amount${this.state.product._id}`} /></div>
                    {this.state.product.countInStock >= 1 ? <button className="add_to_car_details" onClick={() => this.Verify(this.state.product._id)}>Add item to cart</button> : <button className="add_disabled_details" disabled>Without Stock</button>}
                    <h5 className="Stock_detail">Stock: {this.state.product.countInStock}</h5>
                </div>
            )
        } else {
            return <h3>The requested product was not found</h3>
        }
    }

    render() {
        var productos = localStorage.Shopping_car === undefined ? [] : JSON.parse(localStorage.Shopping_car);
        var amount_products = productos.length;
        return (
            <div className="container_details">
                <div className="carrito">
                    <a href="http://localhost:3000/shopping_car" className="Cart_Count">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                            <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                        </svg>
                        ({amount_products})
                    </a>
                </div>
                {this.Product()}
            </div>
        );
    }
}

export default Show_Details;