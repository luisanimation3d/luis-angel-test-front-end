import React, { Component } from 'react';

import '.././css/carrito.css';

class Shopping_car extends Component {

    Restar(id) {
        var producto = localStorage.Shopping_car === null ? [] : JSON.parse(localStorage.Shopping_car);
        if (producto.length !== 0) {
            var resultado = producto.find(function (e) { return e.id === id; });
            if (resultado !== undefined) {
                var position = producto.findIndex(function (e) { return e.id === id; });
            }
            var resta_resultado = document.querySelector("#" + producto[position].result);

            var resta_final = parseFloat(producto[position].amount);

            if (resta_final > 1) {
                producto[position].amount = resta_final - 1;
                resta_resultado.value = producto[position].amount;
                producto[position].total = producto[position].amount * producto[position].price;
                localStorage.Shopping_car = JSON.stringify(producto);
                this.Read_car();
                window.location.reload();
            } else {
                alert("The minimum quantity is 1, if you won't get the product, you can drop it");
            }
        }
    };

    Sumar(id) {
        var producto = localStorage.Shopping_car === null ? [] : JSON.parse(localStorage.Shopping_car);
        if (producto.length !== 0) {
            var resultado = producto.find(function (e) { return e.id === id; });
            if (resultado !== undefined) {
                var position = producto.findIndex(function (e) { return e.id === id; });
            }
            var Suma_resultado = document.querySelector("#" + producto[position].result);

            var Suma_final = parseFloat(producto[position].amount) + 1;

            var Stock = parseFloat(producto[position].stock);

            if (Suma_final <= Stock) {
                producto[position].amount = producto[position].amount + 1;
                Suma_resultado.value = producto[position].amount;
                producto[position].total = producto[position].amount * producto[position].price;
                localStorage.Shopping_car = JSON.stringify(producto);
                this.Read_car();
                window.location.reload();
            } else {
                alert("Not enough stock");
            }
        }

    };

    Eliminar(id) {
        var producto = localStorage.Shopping_car === null ? [] : JSON.parse(localStorage.Shopping_car);
        if (producto.length !== 0) {
            var producto_eliminar = producto.findIndex(function (e) { return e.id === id; });
            if (producto_eliminar !== -1) {
                producto.splice(producto_eliminar, 1);
                localStorage.Shopping_car = JSON.stringify(producto);
                this.Read_car();
                window.location.reload();
            }
            else {
                alert("Product not found");
            }
        }
    };

    Read_car() {
        var productos = localStorage.Shopping_car === null ? [] : JSON.parse(localStorage.Shopping_car);
        if (productos.length !== 0) {

            return productos.map((e, i) => {
                return (
                    <div className="card_shopping" key={i}>
                        <img src={`http://localhost:5000${e.img}`} alt={e.name} />
                        <h2 className="product_name">{e.name}</h2>
                        <p className="product_description">{e.description}</p>
                        <p className="product_details"><span>Marca:</span> {e.brand}<span> Categoria:</span> {e.category}</p>
                        <h3 className="price">${e.total.toFixed(2)}</h3>
                        <div className="amount"><button onClick={() => this.Restar(e.id)}>-</button><input readOnly id={e.result} value={e.amount} /><button onClick={() => this.Sumar(e.id)}>+</button></div>
                        <h5 className="Stock">Stock: {e.stock}</h5>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="delete" onClick={() => this.Eliminar(e.id)} viewBox="0 0 16 16">
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                    </div>)
            });

        } else {
            return (
                <div>
                    <h3>The cart is empty, <a href="http://localhost:3000/catalogo" className="link_catalogo"> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                        <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                    </svg> Let's go shopping!</a></h3>
                </div>
            )
        }
    }

    render() {
        var productos = localStorage.Shopping_car === undefined ? [] : JSON.parse(localStorage.Shopping_car);
        var amount_products = productos.length;
        var valor_total = 0;
        productos.forEach((e) =>{

            valor_total = valor_total + e.total;

        });
        return (
            <div className="container">
                <div className="carrito">
                    <a href="http://localhost:3000/shopping_car" className="Cart_Count">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                            <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                        </svg>
                        ({amount_products})
                    </a>
                </div>
                {this.Read_car()}

                {valor_total > 0 ? <h5 className="Total">Total: ${valor_total}</h5> : ""}
            </div>
        );
    }
}

export default Shopping_car;

